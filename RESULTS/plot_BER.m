%%
% Load Values
clear all
clc
dataValues=importdata('BERvsdBm_widebus.csv');
%%
% Plot Values
X=dataValues.data(:,1);
Y=dataValues.data(:,2);
X1=[X ; [-10 : 0.5 : -5]'];
Y1=[Y;zeros(11,1)];
Y1=Y1/10^(12); % BER
Z1=log10(Y1);

% Create semilogy
% semilogy(X1,Y1,'Marker','*','LineStyle','none','Color',[1 0 0]);

%% 
% Data Fitting 
%% Fit: 'untitled fit 1'.
[xData, yData] = prepareCurveData( X1, Z1 );

% Set up fittype and options.
ft = fittype( 'exp1' );
opts = fitoptions( ft );
opts.Display = 'Off';
opts.Lower = [-Inf -Inf];
opts.Robust = 'Bisquare';
opts.StartPoint = [-13.9258664501566 0.350325630211638];
opts.Upper = [Inf Inf];
opts.Normalize = 'on';

% Fit model to data.
[fitresult, gof] = fit( xData, yData, ft, opts );

% Plot fit with data.
figure1=figure( 'Name', 'untitled fit 1' );

% Create axes
axes1 = axes('Parent',figure1,'YMinorTick','on',...
    'FontWeight','demi',...
    'FontSize',20);
hold(axes1,'all');

h = plot( fitresult, xData, yData);
legend( h, 'Data Readings', 'Fitted Curve', 'Location', 'NorthEast' );
% Label axes
xlabel({'\leftarrow Power (dBm) \rightarrow'},'FontWeight','bold','FontSize',24);
ylabel({'\leftarrow Bit Error Ratio (BER) in log scale\rightarrow'},'FontWeight','bold','FontSize',24);
grid on


%% To overlap the other GBT BER
%%
% Load Values
dataValues=importdata('BERvsdBm_GBT.csv');
%%
% Plot Values
X=dataValues.data(:,1);
Y=dataValues.data(:,2);
X1=[X ; [-10 : 0.5 : -5]'];
Y1=[Y;zeros(11,1)];
Y1=Y1/10^(12); % BER
Z1=log10(Y1);

% Create semilogy
% semilogy(X1,Y1,'Marker','*','LineStyle','none','Color',[1 0 0]);

%% 
% Data Fitting 
%% Fit: 'untitled fit 1'.
[xData, yData] = prepareCurveData( X1, Z1 );

% Set up fittype and options.
ft = fittype( 'exp1' );
opts = fitoptions( ft );
opts.Display = 'Off';
opts.Lower = [-Inf -Inf];
opts.Robust = 'Bisquare';
opts.StartPoint = [-13.9258664501566 0.350325630211638];
opts.Upper = [Inf Inf];
opts.Normalize = 'on';

% Fit model to data.
[fitresult, gof] = fit( xData, yData, ft, opts );

% Plot fit with data.
% figure1=figure( 'Name', 'untitled fit 1' );
% 
% Create axes
% axes1 = axes('Parent',figure1,'YMinorTick','on',...
%     'FontWeight','demi',...
%     'FontSize',20);
% hold(axes1,'all');

h = plot( fitresult, xData, yData );
legend( h, 'Data Readings', 'Fitted Curve', 'Location', 'NorthEast' );

% Label axes
xlabel({'\leftarrow Power (dBm) \rightarrow'},'FontWeight','bold','FontSize',24);
ylabel({'\leftarrow Bit Error Ratio (BER) in log scale\rightarrow'},'FontWeight','bold','FontSize',24);
grid on


