
%%
% REF:
% 1) https://indico.cern.ch/event/49682/contributions/1175755/attachments/961620/1365135/FPGA_Soos.pdf
% 2) http://indico.cern.ch/event/49682/contributions/1175755/attachments/961619/1365132/FPGA-based_Bit_Error_Rate_Tester.pdf
% 3) http://notes-application.abcelectronique.com/003/3-5321.pdf
% 4) http://ieeexplore.ieee.org.sci-hub.cc/document/1363708/

clear all
clc

% CONFIDENCE LEVEL
CL  = 0.99;

% DATA RATE
R = 4.8 * 10^9;

% Number of Transmitted bits
n = 0;

% Number of Error Bits
N = 0;

% BIT ERROR RATE
BER = 10^(-12);

% Measurement Time
T = 0;

%%
% LATEX EQUATION
% 
% $$n = -\frac{\ln(1-CL)}{BER} + \frac{\ln\left(\sum_{k=0}^{N} \frac{(n*BER)^k}{k!}\right)}{BER}$$
%
% $$T = n/R$$
% 

n = - (log(1-CL)/BER);
 for k = 0:1:N
    n = n  + ((log(((n*BER)^k)/(factorial(k))))/BER) ;
 end
T = n / R;

fprintf('Number of Transmitted Bits = %d\n', n);
fprintf('Time Taken = %d secs \n', T);



%% 
% Total Time vs Line Rate 
%
%
%
CL  = 0.90;
R = 2:0.02:10; R=R*(10^9);


% Create figure
plot_fig = figure;

% Create axes
axes1 = axes('Parent',plot_fig,'YMinorTick','on',...
    'XTickLabel',{'2','3','4','5','6','7','8','9','10'},...
    'XTick',[2000000000 3000000000 4000000000 5000000000 6000000000 7000000000 8000000000 9000000000 10000000000],...
    'XMinorTick','on',...
    'FontWeight','demi',...
    'FontSize',16);
box(axes1,'on');
hold(axes1,'all');

n = - (log(1-CL)/BER);
 for k = 0:1:N
    n = n  + ((log(((n*BER)^k)/(factorial(k))))/BER) ;
 end
T =n./R;
plot(R,T,'blue');
hold on

CL  = 0.95;
n = - (log(1-CL)/BER);
 for k = 0:1:N
    n = n  + ((log(((n*BER)^k)/(factorial(k))))/BER) ;
 end
T =n./R;
plot(R,T,'red');
hold on

CL  = 0.99;
n = - (log(1-CL)/BER);
 for k = 0:1:N
    n = n  + ((log(((n*BER)^k)/(factorial(k))))/BER) ;
 end
T =n./R;
plot(R,T,'green');


% Create xlabel
xlabel({'\leftarrow Line Rate (Gbps) \rightarrow'},'FontWeight','bold',...
    'FontSize',16);

% Create ylabel
ylabel({'Test Time needed to reach 10^{-12} with no errors (secs) \rightarrow'},...
    'FontWeight','bold',...
    'FontSize',16);

% Create legend
legend1=legend('CL = 0.90','CL = 0.95','CL = 0.99');
set(legend1,...
    'Position',[0.56792534722222 0.670244466805692 0.135416666666667 0.176413255360624],...
    'FontSize',20);

% Create text
text('Parent',axes1,'String','GBT line rate = 4.8 Gbps',...
    'Position',[4840336134.45378 2040.770609319 17.3205080756888],...
    'FontWeight','demi',...
    'FontSize',12);

% Create text
text('Parent',axes1,'String','TTC-PON upstream rate = 2.4 Gbps',...
    'Position',[2426890756.30252 2363.35125448029 17.3205080756888],...
    'FontWeight','demi',...
    'FontSize',12);

% Create text
text('Parent',axes1,'String','TTC-PON downstream rate = 9.6 Gbps',...
    'Position',[7610084033.61345 2345.43010752688 17.3205080756888],...
    'FontWeight','demi',...
    'FontSize',12);

% Create legend
legend1 = legend(axes1,'show');
set(legend1,...
    'Position',[0.703342013888885 0.519342691719006 0.115885416666667 0.15448343079922],...
    'FontSize',20);

% Create line
annotation(plot_fig,'line',[0.401041666666667 0.401041666666667],...
    [0.111111111111111 0.922563417890521],'LineStyle',':');

% Create line
annotation(plot_fig,'line',[0.169088541666667 0.169088541666667],...
    [0.111578947368421 0.923031254147831],'LineStyle',':');

% Create line
annotation(plot_fig,'line',[0.866354166666667 0.866354166666667],...
    [0.10719298245614 0.91864528923555],'LineStyle',':');