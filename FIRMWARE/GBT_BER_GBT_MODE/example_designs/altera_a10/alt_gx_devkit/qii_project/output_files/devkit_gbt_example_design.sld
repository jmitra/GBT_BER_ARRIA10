<sld_project_info>
  <sld_infos>
    <sld_info hpath="alt_ax_gbt_example_design:gbtExmplDsgn_inst|alt_a10_reset:gbtBank1_genRst|alt_a10_lpm_shiftreg:rst_gen_slr" name="rst_gen_slr">
      <assignment_values>
        <assignment_value text="QSYS_NAME alt_a10_lpm_shiftreg HAS_SOPCINFO 1 GENERATION_ID 1492510375"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="alt_ax_gbt_example_design:gbtExmplDsgn_inst|gbt_bank:gbtBank|gbt_rx:\gbtRx_param_generic_src_gen:gbtRx_gen:1:gbtRx|gbt_rx_gearbox:rxGearbox|gbt_rx_gearbox_std:\rxGearboxStd_gen:rxGearboxStd|gbt_rx_gearbox_std_dpram:dpram|alt_ax_rx_dpram:dpram" name="dpram">
      <assignment_values>
        <assignment_value text="QSYS_NAME alt_ax_rx_dpram HAS_SOPCINFO 1 GENERATION_ID 1492510116"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="alt_ax_gbt_example_design:gbtExmplDsgn_inst|gbt_bank:gbtBank|gbt_tx:\gbtTx_param_generic_src_gen:gbtTx_gen:1:gbtTx|gbt_tx_gearbox:txGearbox|gbt_tx_gearbox_std:\txGearboxStd_gen:txGearboxStd|gbt_tx_gearbox_std_dpram:dpram|alt_ax_tx_dpram:dpram" name="dpram">
      <assignment_values>
        <assignment_value text="QSYS_NAME alt_ax_tx_dpram HAS_SOPCINFO 1 GENERATION_ID 1492510122"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="ax_data_issp:gbtBank1_Data_inSysSrcAndPrb" name="gbtBank1_Data_inSysSrcAndPrb">
      <assignment_values>
        <assignment_value text="QSYS_NAME ax_data_issp HAS_SOPCINFO 1 GENERATION_ID 1493040573"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="ax_issp:gbtBank1_inSysSrcAndPrb" name="gbtBank1_inSysSrcAndPrb">
      <assignment_values>
        <assignment_value text="QSYS_NAME ax_issp HAS_SOPCINFO 1 GENERATION_ID 1492510393"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="frameclk_pll:frameclk_pll_inst" name="frameclk_pll_inst">
      <assignment_values>
        <assignment_value text="QSYS_NAME frameclk_pll HAS_SOPCINFO 1 GENERATION_ID 1492510387"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="alt_ax_gbt_example_design:gbtExmplDsgn_inst|gbt_bank:gbtBank|multi_gigabit_transceivers:\mgt_param_generic_src_gen:mgt|mgt_std:\mgtStd_gen:mgtStd|alt_sv_mgt_resetctrl:\gxRstCtrl_gen:1:gxRstCtrl|gx_reset_rx:gxResetRx" name="gxResetRx">
      <assignment_values>
        <assignment_value text="QSYS_NAME gx_reset_rx HAS_SOPCINFO 1 GENERATION_ID 1492510338"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="alt_ax_gbt_example_design:gbtExmplDsgn_inst|gbt_bank:gbtBank|multi_gigabit_transceivers:\mgt_param_generic_src_gen:mgt|mgt_std:\mgtStd_gen:mgtStd|alt_sv_mgt_resetctrl:\gxRstCtrl_gen:1:gxRstCtrl|gx_reset_tx:gxResetTx" name="gxResetTx">
      <assignment_values>
        <assignment_value text="QSYS_NAME gx_reset_tx HAS_SOPCINFO 1 GENERATION_ID 1492510345"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="alt_ax_gbt_example_design:gbtExmplDsgn_inst|gbt_bank:gbtBank|multi_gigabit_transceivers:\mgt_param_generic_src_gen:mgt|mgt_std:\mgtStd_gen:mgtStd|gx_std_x1:\gxStd_x1_gen:gxStd_x1" name="\gxStd_x1_gen:gxStd_x1">
      <assignment_values>
        <assignment_value text="QSYS_NAME gx_std_x1 HAS_SOPCINFO 1 GENERATION_ID 1492510250"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="alt_ax_gbt_example_design:gbtExmplDsgn_inst|gbt_bank:gbtBank|multi_gigabit_transceivers:\mgt_param_generic_src_gen:mgt|mgt_std:\mgtStd_gen:mgtStd|alt_ax_mgt_txpll:mgt_tx_pll|mgt_atxpll:\ATXpll_gen:transceiver_atxpll" name="\ATXpll_gen:transceiver_atxpll">
      <assignment_values>
        <assignment_value text="QSYS_NAME mgt_atxpll HAS_SOPCINFO 1 GENERATION_ID 1492510130"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="alt_ax_gbt_example_design:gbtExmplDsgn_inst|gbt_bank:gbtBank|multi_gigabit_transceivers:\mgt_param_generic_src_gen:mgt|mgt_std:\mgtStd_gen:mgtStd|alt_ax_mgt_txpll:mgt_tx_pll|mgt_tx_pll_rst:transceiver_atxpll_rst" name="transceiver_atxpll_rst">
      <assignment_values>
        <assignment_value text="QSYS_NAME mgt_tx_pll_rst HAS_SOPCINFO 1 GENERATION_ID 1492510352"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="sld_hub:auto_hub|alt_sld_fab:\instrumentation_fabric_with_node_gen:instrumentation_fabric" library="alt_sld_fab" name="instrumentation_fabric">
      <assignment_values>
        <assignment_value text="QSYS_NAME alt_sld_fab HAS_SOPCINFO 1"/>
      </assignment_values>
    </sld_info>
  </sld_infos>
</sld_project_info>
